FROM microsoft/dotnet:latest
WORKDIR /app

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c bin/Release/netcoreapp2.1 -o out
ENTRYPOINT ["dotnet", "out/myTestApp.dll"]